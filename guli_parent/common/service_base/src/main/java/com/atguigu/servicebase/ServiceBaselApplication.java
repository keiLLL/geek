package com.atguigu.servicebase;

import org.springframework.boot.SpringApplication;


public class ServiceBaselApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceBaselApplication.class, args);
    }
}
