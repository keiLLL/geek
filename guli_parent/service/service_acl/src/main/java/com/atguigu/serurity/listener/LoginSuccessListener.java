package com.atguigu.serurity.listener;

import com.atguigu.aclservice.entity.SystemLogLoginEntity;
import com.atguigu.aclservice.mapper.SystemLogLoginMapper;
import com.atguigu.commonutils.ordervo.IpUtils;
import com.atguigu.serurity.entity.SecurityUser;
import com.atguigu.serurity.enums.LoginOperationEnum;
import com.atguigu.serurity.enums.LoginStatusEnum;
import org.apache.http.HttpHeaders;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class LoginSuccessListener implements ApplicationListener<AuthenticationSuccessEvent> {

    @Resource
    private SystemLogLoginMapper systemLogLoginMapper;

    @Resource
    private HttpServletRequest req;

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent event) {
        SecurityUser user = (SecurityUser) event.getAuthentication().getPrincipal();
        SystemLogLoginEntity sysLogLoginEntity = new SystemLogLoginEntity();
        sysLogLoginEntity.setOperation(LoginOperationEnum.LOGIN.value());
        sysLogLoginEntity.setCreateDate(new Date());
        sysLogLoginEntity.setIp(IpUtils.getIpAddr(req));
        sysLogLoginEntity.setUserAgent(req.getHeader(HttpHeaders.USER_AGENT));
        sysLogLoginEntity.setIp(IpUtils.getIpAddr(req));
        sysLogLoginEntity.setStatus(LoginStatusEnum.SUCCESS.value());
        sysLogLoginEntity.setCreator(Long.parseLong(user.getCurrentUserInfo().getId()));
        sysLogLoginEntity.setCreatorName(user.getCurrentUserInfo().getUsername());
        systemLogLoginMapper.insert(sysLogLoginEntity);
    }
}
