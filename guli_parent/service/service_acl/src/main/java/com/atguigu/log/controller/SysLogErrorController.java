/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.atguigu.log.controller;

import com.atguigu.common.annotation.LogOperation;
import com.atguigu.commonutils.R;
import com.atguigu.log.dto.SysLogErrorDTO;
import com.atguigu.log.excel.SysLogErrorExcel;
import com.atguigu.log.service.SysLogErrorService;
import com.atguigu.servicebase.page.PageData;
import com.atguigu.servicebase.utils.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 异常日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@RestController
@RequestMapping("admin/log/error")
@Api(tags="异常日志")
public class SysLogErrorController {
    @Autowired
    private SysLogErrorService sysLogErrorService;
    @GetMapping("page")
//    @LogOperation("查看")
//    @RequiresPermissions("sys:log:error")
    public R<PageData<SysLogErrorDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<SysLogErrorDTO> page = sysLogErrorService.page(params);
        long total= page.getTotal();
        return R.ok().data("total",total).data("rows",page.getList());
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
//    @RequiresPermissions("sys:log:error")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<SysLogErrorDTO> list = sysLogErrorService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, SysLogErrorExcel.class);
    }

}