/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.atguigu.log.dao;

import com.atguigu.log.entity.SysLogErrorEntity;
import com.atguigu.servicebase.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 异常日志
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0
 */
@Mapper
@Repository
public interface SysLogErrorDao extends BaseDao<SysLogErrorEntity> {
	
}
