package com.atguigu.aclservice.service;

import com.atguigu.aclservice.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserService extends IService<SysUser> {

    // 从数据库中取出用户信息
    SysUser selectByUsername(String username);
    SysUser getById(String id);
}
