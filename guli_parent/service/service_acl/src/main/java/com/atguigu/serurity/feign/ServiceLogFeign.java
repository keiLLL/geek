package com.atguigu.serurity.feign;

import com.atguigu.serurity.entity.SysLogLoginEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author : lcw
 * @version : 1.0
 * <P> </P>
 * @date : 2022/11/20
 */

@FeignClient("service-log")
public interface ServiceLogFeign {

    @PostMapping("sys/log/login/save")
    void savaLog(SysLogLoginEntity entity);

}
