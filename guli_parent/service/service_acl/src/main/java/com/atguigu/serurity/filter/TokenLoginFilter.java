package com.atguigu.serurity.filter;

import com.atguigu.aclservice.entity.SystemLogLoginEntity;

import com.atguigu.commonutils.R;
import com.atguigu.commonutils.ResponseUtil;
import com.atguigu.commonutils.ordervo.IpUtils;
import com.atguigu.serurity.entity.SecurityUser;

import com.atguigu.serurity.entity.User;
import com.atguigu.serurity.enums.LoginOperationEnum;
import com.atguigu.serurity.enums.LoginStatusEnum;
import com.atguigu.serurity.feign.ServiceLogFeign;
import com.atguigu.serurity.security.TokenManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * <p>
 * 登录过滤器，继承UsernamePasswordAuthenticationFilter，对用户名密码进行登录校验
 * </p>
 *
 * @author qy
 * @since 2022-10-22
 */
public class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {


    private AuthenticationManager authenticationManager;

    private TokenManager tokenManager;

    private RedisTemplate redisTemplate;

    private ServiceLogFeign feign;

    public TokenLoginFilter(AuthenticationManager authenticationManager, TokenManager tokenManager, RedisTemplate redisTemplate,ServiceLogFeign feign) {
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
        this.feign=feign;
        this.setPostOnly(false);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/admin/acl/login", "POST"));
    }

    @Override
    //封装http请求
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException {
        try {
            User user = new ObjectMapper().readValue(req.getInputStream(), User.class);
            //执行之后调用UserDetalisServicelmpl
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), new ArrayList<>()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 登录成功
     *
     * @param req
     * @param res
     * @param chain
     * @param auth
     * @throws IOException
     * @throws ServletException
     */
    //UerDetalisServicelmpl attemptAuthentication()方法执行完毕
    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
        SecurityUser user = (SecurityUser) auth.getPrincipal();
        String token = tokenManager.createToken(user.getCurrentUserInfo().getUsername());
        redisTemplate.opsForValue().set(user.getCurrentUserInfo().getUsername(), user.getPermissionValueList());

        ResponseUtil.out(res, R.ok().data("token", token));
        SystemLogLoginEntity sysLogLoginEntity = new SystemLogLoginEntity();
        sysLogLoginEntity.setOperation(LoginOperationEnum.LOGIN.value());
        sysLogLoginEntity.setCreateDate(new Date());
        sysLogLoginEntity.setIp(IpUtils.getIpAddr(req));
        sysLogLoginEntity.setUserAgent(req.getHeader(HttpHeaders.USER_AGENT));
        sysLogLoginEntity.setIp(IpUtils.getIpAddr(req));
        sysLogLoginEntity.setStatus(LoginStatusEnum.SUCCESS.value());
        sysLogLoginEntity.setCreator(Long.parseLong(user.getCurrentUserInfo().getId()));
        sysLogLoginEntity.setCreatorName(user.getCurrentUserInfo().getUsername());
//        feign.savaLog(sysLogLoginEntity);
    }

    /**
     * 登录失败
     *
     * @param request
     * @param response
     * @param e
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException e) throws IOException, ServletException {
        ResponseUtil.out(response, R.error());
    }
}
