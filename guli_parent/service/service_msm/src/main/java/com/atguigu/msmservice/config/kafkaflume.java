package com.atguigu.msmservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
@Configuration
public class kafkaflume {
    private static final String CONSUMER_TOPIC="flume";
    @KafkaListener(topics = CONSUMER_TOPIC)
    public void consumerKafkaTopic(String msg){
        System.out.println("來自于本机的信息是："+msg);

    }
}
