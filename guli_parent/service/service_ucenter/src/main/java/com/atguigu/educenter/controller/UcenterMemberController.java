package com.atguigu.educenter.controller;


import com.alibaba.fastjson.JSON;
import com.atguigu.commonutils.JwtUtils;
import com.atguigu.commonutils.MD5;
import com.atguigu.commonutils.R;
import com.atguigu.commonutils.ordervo.UcenterMemberOrder;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.PasswordVo;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static com.atguigu.educenter.config.AESUtil.aesDecrypt;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-03-09
 */
@RestController
@RequestMapping("/educenter/member")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;

    //登录
    @PostMapping("login")
    public R loginUser(@Valid @RequestBody UcenterMember member) {
        //member对象封装手机号和密码
        //调用service方法实现登录
        //返回token值，使用jwt生成
        String password = member.getPassword();
        member.setPassword(aesDecrypt(password));
        String token = memberService.login(member);
        return R.ok().data("token",token);
    }

    //注册
    @PostMapping("register")
    public R registerUser(@Valid @RequestBody RegisterVo registerVo) {
        String password = registerVo.getPassword();
        registerVo.setPassword(aesDecrypt(password));
        memberService.register(registerVo);
        return R.ok();
    }

    //根据token获取用户信息
    @GetMapping("getMemberInfo")
    public R getMemberInfo(HttpServletRequest request) {
        //调用jwt工具类的方法。根据request对象获取头信息，返回用户id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //查询数据库根据用户id获取用户信息
        UcenterMember member = memberService.getById(memberId);
        return R.ok().data("userInfo", member);
    }

    //根据用户id获取用户信息
    @PostMapping("getUserInfoOrder/{id}")
    public UcenterMemberOrder getUserInfoOrder(@PathVariable String id) {
        UcenterMember member = memberService.getById(id);
        //把member对象里面值复制给UcenterMemberOrder对象
        UcenterMemberOrder ucenterMemberOrder = new UcenterMemberOrder();
        BeanUtils.copyProperties(member,ucenterMemberOrder);
        return ucenterMemberOrder;
    }

    //查询某一天注册人数
    @GetMapping("countRegister/{day}")
    public R countRegister(@PathVariable String day) {
        Integer count = memberService.countRegisterDay(day);
        return R.ok().data("countRegister",count);
    }
    //修改用户信息
    @PostMapping("/update")
    public R updatesession(@RequestBody UcenterMember ucenterMember){
        memberService.updateById(ucenterMember);
        return R.ok();
    }

    //修改用户密码
    @PostMapping("/updatepassword")
    public R updatepassword(@RequestBody PasswordVo passwordVo){
        String newpassword = passwordVo.getNewPassword();
        passwordVo.setNewPassword(aesDecrypt(newpassword));
        String oldpassword = passwordVo.getOldPassword();
        passwordVo.setOldPassword(aesDecrypt(oldpassword));
        UcenterMember ucenterMember = memberService.getById(passwordVo.getId());
        if(!MD5.encrypt(passwordVo.getOldPassword()).equals(ucenterMember.getPassword())) {
            throw new GuliException(20001,"旧密码错误");
        }
        String Password = passwordVo.getNewPassword();
        ucenterMember.setPassword(MD5.encrypt(Password));
        memberService.updateById(ucenterMember);
        return R.ok();
    }
}

