package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacher2Service;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/indexfront")
//@CrossOrigin
public class IndexFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;
    @Autowired
    private EduTeacher2Service teacherService2;

    //查询前8条热门课程，查询前4条名师,查询前4张证书
    @GetMapping("/index")
    public R index() {
        //查询前8条热门课程
//        List<EduCourse> eduList = courseService.getlist1();
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("buy_count");
        wrapper.last("limit 8");
        List<EduCourse> eduList = courseService.list(wrapper);

        //查询前4条名师
//        List<EduTeacher> teacherList = teacherService.getlist2();
        QueryWrapper<EduTeacher> wrapperTeacher = new QueryWrapper<>();
        wrapperTeacher.orderByDesc("level");
        wrapperTeacher.last("limit 4");
        List<EduTeacher> teacherList = teacherService.list(wrapperTeacher);

        //查询前4张证书
        QueryWrapper<EduTeacher2> wrapperTeacher2 = new QueryWrapper<>();
        wrapperTeacher2.orderByDesc("view_count");
        wrapperTeacher2.last("limit 4");
        List<EduTeacher2> eduList2 = teacherService2.list(wrapperTeacher2);
        Map<String,Object> map = new HashMap<>();
        map.put("teacherList",teacherList);
        map.put("eduList",eduList);
        map.put("eduList2",eduList2);
        return R.ok().data(map);
    }

}
