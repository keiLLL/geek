package com.atguigu.eduservice.entity.Form;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher2Form {
    private String id;

    private String name2;

    private String intro2;

    private String career2;

    private Integer level2;

}
