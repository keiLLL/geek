package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.Teacher2InfoVo;
import com.atguigu.eduservice.entity.vo.Teacher2Query;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
public interface EduTeacher2Service extends IService<EduTeacher2> {

    Map<String, Object> getAllTeacher2FrontList(Page<EduTeacher2> pageTeacher2);

    Map<String, Object> getTeacher2FrontList(Page<EduTeacher2> pageTeacher2, Teacher2Query teacher2Query);

    EduTeacher2 getTeacher2ById(String id2);

    Teacher2InfoVo getTeacher2Info(String id2);
    void updateTeacher2Info(Teacher2InfoVo teacher2InfoVo);

    Map<String, Object> getTeacher2FrontList2(Page<EduTeacher2> pageTeacher2, Teacher2Query teacher2Query);
}
