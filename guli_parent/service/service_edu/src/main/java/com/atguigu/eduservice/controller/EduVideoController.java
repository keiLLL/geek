package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-03-02
 */
@RestController
@RequestMapping("/eduservice/video")
//@CrossOrigin
public class EduVideoController {

    @Autowired
    private EduVideoService VideoService;
    //注入VodClient
    @Autowired
    private VodClient vodClient;
    //添加小节
    @PostMapping("")
    public R addVideo(@RequestBody EduVideo eduVideo){
        VideoService.save(eduVideo);
        return R.ok();
    }

    //删除小节
    @DeleteMapping("{id}")
    public R deleteVideo(@PathVariable String id){
        //根据小节id查询，得到视频id,然后删除视频
        EduVideo eduVideo = VideoService.getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        if(!StringUtils.isEmpty(videoSourceId)){
            //根据视频id远程调用删除视频
            R r = vodClient.removeAlyVideo(videoSourceId);
            if(r.getCode() == 20001) {
                throw new GuliException(20001, "删除视频失败,熔断器....");
            }
        }
        //删除小节
        VideoService.removeById(id);
        return R.ok();
    }
    //修改小节
    @PostMapping("/update")
    public R updateVideo(@RequestBody EduVideo eduVideo){
        VideoService.updateById(eduVideo);
        return R.ok();
    }
    @GetMapping("getVideo/{id}")
    public R getVideo(@PathVariable String id) {
        EduVideo eduVideo = VideoService.getById(id);
        return R.ok().data("video", eduVideo);
    }
}

