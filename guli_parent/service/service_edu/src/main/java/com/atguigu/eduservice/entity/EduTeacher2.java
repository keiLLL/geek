package com.atguigu.eduservice.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 讲师
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="EduTeacher2对象", description="讲师")
public class EduTeacher2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "证书ID")
    @TableId(value = "id2", type = IdType.ASSIGN_ID)
    private String id2;

    @ApiModelProperty(value = "证书名称")
    @NotBlank(message = "证书名不能为空")
    private String name2;

    @ApiModelProperty(value = "证书简介")
    @NotBlank(message = "证书简介不能为空")
    private String intro2;

    @ApiModelProperty(value = "证书介绍,一句话说明证书")
    private String career2;

    @ApiModelProperty(value = "证书等级")
    private Integer level2;

    @ApiModelProperty(value = "证书封面")
    private String avatar2;

    @ApiModelProperty(value = "热度")
    private long viewCount;

    @ApiModelProperty(value = "备考信息")
    private String preparation;

    @ApiModelProperty(value = "排序")
    private Integer sort2;

    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted2;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;


}
