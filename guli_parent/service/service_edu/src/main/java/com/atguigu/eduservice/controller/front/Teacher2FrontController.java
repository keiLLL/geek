package com.atguigu.eduservice.controller.front;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.entity.frontvo.CourseFrontVo;
import com.atguigu.eduservice.entity.vo.Teacher2Query;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacher2Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/eduservice/teacher2front")
public class Teacher2FrontController {
    @Autowired
    private EduTeacher2Service eduTeacher2Service;
    @Autowired
    EduCourseService courseService;
    private String teacherid2;

    //全部分页展示证书列表
    @PostMapping("All/{page}/{limit}")
    public R getAllTeacher2(@PathVariable long page,@PathVariable long limit) {
        Page<EduTeacher2> pageTeacher2 = new Page<>(page,limit);
        Map<String,Object> map = eduTeacher2Service.getAllTeacher2FrontList(pageTeacher2);
        //返回分页所有数据
        return R.ok().data(map);
    }
    //1 条件查询带分页查询证书
    @PostMapping("{page}/{limit}")
    public R getFrontTeacherList(@PathVariable long page, @PathVariable long limit,@RequestBody(required = false) Teacher2Query teacher2Query) {
        Page<EduTeacher2> pageTeacher2 = new Page<>(page,limit);
        Map<String,Object> map = eduTeacher2Service.getTeacher2FrontList(pageTeacher2,teacher2Query);
        //返回分页所有数据
        return R.ok().data(map);
    }

    @PostMapping("backend/{page}/{limit}")
    public R getbackendTeacherList(@PathVariable long page, @PathVariable long limit,@RequestBody(required = false) Teacher2Query teacher2Query) {
        Page<EduTeacher2> pageTeacher2 = new Page<>(page,limit);
        Map<String,Object> map = eduTeacher2Service.getTeacher2FrontList2(pageTeacher2,teacher2Query);
        //返回分页所有数据
        return R.ok().data(map);
    }
    //2 根据证书id，查询相关课程
//    @GetMapping("{page}/{limit}/{teacherid2}")
//    public R getTeacher2ById(@PathVariable long page, @PathVariable long limit,@PathVariable String teacherid2){
//
//        Page<EduCourse> pageCourse = new Page<>(page,limit);
//        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
//        wrapper.eq("id2",teacherid2);
//        courseService.page(pageCourse,wrapper);
//        long total = pageCourse.getTotal();//总记录数
//        List<EduCourse> records = pageCourse.getRecords();
//        Map map= new HashMap();
//        map.put("total",total);
//        map.put("rows",records);
//        return R.ok().data(map);
//    }
    //根据证书id查询相关课程
    @GetMapping("getCourseTeacher2/{teacherid2}")
    public R getCourseTeacher2ById(@PathVariable String teacherid2){
        long page = 1;
        long limit = 10;
        Page<EduCourse> pageCourse = new Page<>(page,limit);
        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("id2",teacherid2);
        courseService.page(pageCourse,wrapper);
        long total = pageCourse.getTotal();//总记录数
        List<EduCourse> records = pageCourse.getRecords();
        Map map= new HashMap();
        map.put("total",total);
        map.put("rows",records);
        return R.ok().data(map);
    }
    //3 根据id查询证书
    @GetMapping("{id2}")
    public R getTeacher2ById(@PathVariable String id2){
        EduTeacher2 eduTeacher2 = eduTeacher2Service.getTeacher2ById(id2);
        return R.ok().data("eduTeacher2",eduTeacher2);
    }

    //4根据id删除证书信息
    @DeleteMapping("{id2}")
    public R deleteTeacher2(@PathVariable String id2){
        boolean flag = eduTeacher2Service.removeById(id2);
        if(flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }
}
