package com.atguigu.eduservice.service;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.RequestParams;

public interface TeacherEsService {
     R search(RequestParams params);
     void deleteById(String Teacher2Id);
     void saveById(String Teacher2Id);
}
