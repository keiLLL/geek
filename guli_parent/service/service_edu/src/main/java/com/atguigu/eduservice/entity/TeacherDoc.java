package com.atguigu.eduservice.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
public class TeacherDoc {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "讲师ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty(value = "讲师姓名")
    private String name;

    @ApiModelProperty(value = "讲师简介")
    private String intro;

    @ApiModelProperty(value = "讲师资历,一句话说明讲师")
    private String career;

    @ApiModelProperty(value = "头衔 1高级讲师 2首席讲师")
    private Integer level;

    @ApiModelProperty(value = "讲师头像")
    private String avatar;

    @ApiModelProperty(value = "排序")
    private Integer sort;
    public TeacherDoc(EduTeacher eduTeacher)
    {
        this.id=eduTeacher.getId();
        this.name=eduTeacher.getName();
        this.intro=eduTeacher.getIntro();
        this.career=eduTeacher.getCareer();
        this.level=eduTeacher.getLevel();
        this.avatar=eduTeacher.getAvatar();
        this.sort=eduTeacher.getSort();
    }
}
