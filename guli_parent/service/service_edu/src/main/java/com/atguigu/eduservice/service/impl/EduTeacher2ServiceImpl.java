package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduCourseDescription;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.Teacher2InfoVo;
import com.atguigu.eduservice.entity.vo.Teacher2Query;
import com.atguigu.eduservice.mapper.EduTeacher2Mapper;
import com.atguigu.eduservice.service.EduTeacher2Service;
import com.atguigu.servicebase.exceptionhandler.GuliException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
@Service
public class EduTeacher2ServiceImpl extends ServiceImpl<EduTeacher2Mapper, EduTeacher2> implements EduTeacher2Service {

    @Autowired
    private EduTeacher2Service eduTeacher2Service;

    @Override
    public Map<String, Object> getAllTeacher2FrontList(Page<EduTeacher2> pageTeacher2) {
        QueryWrapper<EduTeacher2> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id2");
        //把分页数据封装到pageTeacher对象
        baseMapper.selectPage(pageTeacher2, wrapper);

        List<EduTeacher2> records = pageTeacher2.getRecords();
        long current = pageTeacher2.getCurrent();
        long pages = pageTeacher2.getPages();
        long size = pageTeacher2.getSize();
        long total = pageTeacher2.getTotal();
        boolean hasNext = pageTeacher2.hasNext();//下一页
        boolean hasPrevious = pageTeacher2.hasPrevious();//上一页

        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        //map返回
        return map;
    }

    @Override
    public Map<String, Object> getTeacher2FrontList(Page<EduTeacher2> pageTeacher2, Teacher2Query teacher2Query) {
        QueryWrapper<EduTeacher2> wrapper = new QueryWrapper<>();
        long level2 = teacher2Query.getLevel2();
//        String name2 = teacher2Query.getName2();
//        if(!StringUtils.isEmpty(name2)) {
//            //构建条件
//            wrapper.like("name2",name2);
//        }
        if(!StringUtils.isEmpty(level2)&&level2!=-1) {
            //构建条件
            wrapper.eq("level2", level2);
        }

        wrapper.orderByDesc("gmt_create");
        eduTeacher2Service.page(pageTeacher2, wrapper);
        List<EduTeacher2> records = pageTeacher2.getRecords();
        long current = pageTeacher2.getCurrent();
        long pages = pageTeacher2.getPages();
        long size = pageTeacher2.getSize();
        long total = pageTeacher2.getTotal();//总记录数
        boolean hasNext = pageTeacher2.hasNext();//下一页
        boolean hasPrevious = pageTeacher2.hasPrevious();//上一页
        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        //map返回
        return map;
    }

    @Override
    public EduTeacher2 getTeacher2ById(String id2) {
        EduTeacher2 eduTeacher2 = baseMapper.selectById(id2);

        return eduTeacher2;
    }

    @Override
    public void updateTeacher2Info(Teacher2InfoVo teacher2InfoVo) {
        EduTeacher2 eduTeacher2 = new EduTeacher2();
        BeanUtils.copyProperties(teacher2InfoVo, eduTeacher2);
        int update = baseMapper.updateById(eduTeacher2);
        if (update == 0) {
            throw new GuliException(20001, "修改课程信息失败");
        }
    }

    @Override
    public Map<String, Object> getTeacher2FrontList2(Page<EduTeacher2> pageTeacher2, Teacher2Query teacher2Query) {
        QueryWrapper<EduTeacher2> wrapper = new QueryWrapper<>();
        String name2 = teacher2Query.getName2();
        if(!StringUtils.isEmpty(name2)) {
            //构建条件
            wrapper.like("name2",name2);
        }

        wrapper.orderByDesc("gmt_create");
        eduTeacher2Service.page(pageTeacher2, wrapper);
        List<EduTeacher2> records = pageTeacher2.getRecords();
        long current = pageTeacher2.getCurrent();
        long pages = pageTeacher2.getPages();
        long size = pageTeacher2.getSize();
        long total = pageTeacher2.getTotal();//总记录数
        boolean hasNext = pageTeacher2.hasNext();//下一页
        boolean hasPrevious = pageTeacher2.hasPrevious();//上一页
        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        //map返回
        return map;
    }

    @Override
    public Teacher2InfoVo getTeacher2Info(String id) {
        //1 查询课程表
        EduTeacher2 teacher2 = baseMapper.selectById(id);
        Teacher2InfoVo teacher2InfoVo = new Teacher2InfoVo();
        BeanUtils.copyProperties(teacher2,teacher2InfoVo);

        return teacher2InfoVo;
    }
}
