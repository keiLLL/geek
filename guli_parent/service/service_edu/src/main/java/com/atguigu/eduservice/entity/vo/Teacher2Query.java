package com.atguigu.eduservice.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class Teacher2Query {
    @ApiModelProperty(value = "证书等级")
    long level2;
    @ApiModelProperty(value = "证书名称")
    String name2;
}
