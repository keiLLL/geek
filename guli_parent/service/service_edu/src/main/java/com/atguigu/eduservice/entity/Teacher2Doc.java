package com.atguigu.eduservice.entity;

import lombok.Data;
@Data
public class Teacher2Doc {

    private String id2;

    private String name2;

    private String intro2;

    private String career2;

    private Integer level2;

    private String avatar2;

    private long viewCount;

    private String preparation;

    private Integer sort2;

    public Teacher2Doc() {
    }

    public Teacher2Doc(EduTeacher2 eduTeacher2)
    {
        this.id2=eduTeacher2.getId2();
        this.name2=eduTeacher2.getName2();
        this.intro2=eduTeacher2.getIntro2();
        this.career2=eduTeacher2.getCareer2();
        this.viewCount=eduTeacher2.getViewCount();
        this.preparation=eduTeacher2.getPreparation();
        this.level2=eduTeacher2.getLevel2();
        this.avatar2=eduTeacher2.getAvatar2();
        this.sort2=eduTeacher2.getSort2();
    }
}
