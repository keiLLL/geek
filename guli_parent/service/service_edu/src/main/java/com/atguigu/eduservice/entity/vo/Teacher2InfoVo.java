package com.atguigu.eduservice.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
@Data
public class Teacher2InfoVo {
    @ApiModelProperty(value = "证书ID")
    private String id2;

    @ApiModelProperty(value = "证书名称")
    private String name2;

    @ApiModelProperty(value = "证书简介")
    private String intro2;

    @ApiModelProperty(value = "证书介绍,一句话说明证书")
    private String career2;

    @ApiModelProperty(value = "证书等级")
    private Integer level2;

    @ApiModelProperty(value = "证书封面")
    private String avatar2;


    @ApiModelProperty(value = "备考信息")
    private String preparation;

}
