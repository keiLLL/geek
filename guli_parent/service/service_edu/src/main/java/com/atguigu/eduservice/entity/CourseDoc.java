package com.atguigu.eduservice.entity;

import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
@Data
public class CourseDoc {
    private String id;

    private String teacherId;

    private String id2;

    private String subjectId;

    private String subjectParentId;
    private String title;
    // 0.01
    private BigDecimal price;

    private Integer lessonNum;

    private String cover;

    private String description;
    public CourseDoc(CourseInfoVo courseInfoVo)
    {
        this.id=courseInfoVo.getId();
        this.teacherId=courseInfoVo.getTeacherId();
        this.id2=courseInfoVo.getId2();
        this.subjectId=courseInfoVo.getSubjectId();
        this.subjectParentId=courseInfoVo.getSubjectParentId();
        this.title=courseInfoVo.getTitle();
        this.price=courseInfoVo.getPrice();
        this.lessonNum=courseInfoVo.getLessonNum();
        this.cover=courseInfoVo.getCover();
        this.description=courseInfoVo.getDescription();
    }
}
