package com.atguigu.eduservice.entity;

import lombok.Data;

@Data
public class RequestParams {
    private String key;
    private Integer page;
    private Integer size;
    private String sortBy;
    //传值实体类
}
