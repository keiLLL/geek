package com.atguigu.eduservice.mapper;

import com.atguigu.eduservice.entity.EduTeacher2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
public interface EduTeacher2Mapper extends BaseMapper<EduTeacher2> {

}
