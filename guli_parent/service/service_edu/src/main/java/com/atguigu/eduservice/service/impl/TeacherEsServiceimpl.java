package com.atguigu.eduservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.entity.RequestParams;
import com.atguigu.eduservice.entity.Teacher2Doc;
import com.atguigu.eduservice.service.EduTeacher2Service;
import com.atguigu.eduservice.service.TeacherEsService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
@Service
public class TeacherEsServiceimpl implements TeacherEsService {
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Autowired
    private EduTeacher2Service eduTeacher2Service;

    @Override
    public R search(RequestParams params) {
        try {
            // 1.准备Request
            SearchRequest request = new SearchRequest("edu_teacher2");
            // 2.准备请求参数
            // 2.1.query
            buildBasicQuery(params, request);
            // 2.2.分页
            int page = params.getPage();
            int size = params.getSize();
            request.source().from((page - 1) * size).size(size);
            // 2.3.距离排序
            // 3.发送请求
            SearchResponse response = restHighLevelClient.search(request, RequestOptions.DEFAULT);
            // 4.解析响应
            return handleResponse(response);
        } catch (IOException e) {
            throw new RuntimeException("搜索数据失败", e);
        }
    }
    private R handleResponse(SearchResponse response) {
        SearchHits searchHits = response.getHits();
        // 4.1.总条数
        long total = searchHits.getTotalHits().value;
//        String total2 = String.valueOf(total);
        // 4.2.获取文档数组
        SearchHit[] hits = searchHits.getHits();
        // 4.3.遍历
        List<Teacher2Doc> teacher2DocList = new ArrayList<>(hits.length);
        for (SearchHit hit : hits) {
            // 4.4.获取source
            String json = hit.getSourceAsString();
            // 4.5.反序列化，非高亮的
           Teacher2Doc teacher2Doc = JSON.parseObject(json, Teacher2Doc.class);
            // 4.6.处理高亮结果
            // 1)获取高亮map
            Map<String, HighlightField> map = hit.getHighlightFields();
            if (map != null && !map.isEmpty()) {
                // 2）根据字段名，获取高亮结果
                HighlightField highlightField = map.get("name");
                if (highlightField != null) {
                    // 3）获取高亮结果字符串数组中的第1个元素
                    String hName = highlightField.getFragments()[0].toString();
                    // 4）把高亮结果放到HotelDoc中
                    teacher2Doc.setName2(hName);
                }
            }
            // 4.9.放入集合
            teacher2DocList.add(teacher2Doc);
        }
        return R.ok().data("total",total).data("items",teacher2DocList);
    }
    private void buildBasicQuery(RequestParams params, SearchRequest request) {
        // 1.准备Boolean查询
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

        // 1.1.关键字搜索，match查询，放到must中
        String key = params.getKey();
        if (StringUtils.isNotBlank(key)) {
            // 不为空，根据关键字查询
            boolQuery.must(QueryBuilders.matchQuery("all", key));
        } else {
            // 为空，查询所有
            boolQuery.must(QueryBuilders.matchAllQuery());
        }

        // 2.算分函数查询
        FunctionScoreQueryBuilder functionScoreQuery = QueryBuilders.functionScoreQuery(
                boolQuery, // 原始查询，boolQuery
                new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{ // function数组
                        new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                                QueryBuilders.termQuery("isAD", true), // 过滤条件
                                ScoreFunctionBuilders.weightFactorFunction(10) // 算分函数
                        )
                }
        );

        // 3.设置查询条件
        request.source().query(functionScoreQuery);
    }
    @Override
    public void deleteById(String Teacher2Id) {
        try {
            // 1.创建request
            DeleteRequest request = new DeleteRequest("edu_teacher2", Teacher2Id);
            // 2.发送请求
            restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException("删除证书数据失败", e);
        }
    }

    @Override
    public void saveById(String Teacher2Id) {
        try {
            // 查询酒店数据，应该基于Feign远程调用hotel-admin，根据id查询酒店数据（现在直接去数据库查）
            EduTeacher2 eduTeacher2 = eduTeacher2Service.getById(Teacher2Id);
            // 转换
            Teacher2Doc teacher2Doc = new Teacher2Doc(eduTeacher2);

            // 1.创建Request
            IndexRequest request = new IndexRequest("edu_teacher2").id(Teacher2Id);
            // 2.准备参数
            request.source(JSON.toJSONString(teacher2Doc), XContentType.JSON);
            // 3.发送请求
            restHighLevelClient.index(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            throw new RuntimeException("新增证书数据失败", e);
        }
    }
}
