package com.atguigu.eduservice.controller;


import com.atguigu.commonutils.R;
import com.atguigu.eduservice.constants.HotelMqConstants;
import com.atguigu.eduservice.entity.EduTeacher2;
import com.atguigu.eduservice.entity.vo.Teacher2InfoVo;
import com.atguigu.eduservice.service.EduTeacher2Service;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2022-12-05
 */
@RestController
@RequestMapping("/eduservice/teacher2")
public class EduTeacher2Controller {
    @Autowired
    private EduTeacher2Service teacher2Service;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    //1 查询证书所有数据
    //rest风格
    @ApiOperation(value = "所有证书列表")
    @GetMapping("")
    public R findAllTeacher2() {
        //调用service的方法实现查询所有的操作
        List<EduTeacher2> list = teacher2Service.list(null);
        return R.ok().data("items",list);
    }
//
//    @GetMapping("")
//    public R findAllTeacher2page()
//    {
//
//    }
    //2 逻辑删除证书的方法
    @ApiOperation(value = "逻辑删除证书")
    @DeleteMapping("{id}")
    public R removeTeacher2(@ApiParam(name = "id", value = "讲师ID", required = true)
                           @PathVariable String id) {
        boolean flag = teacher2Service.removeById(id);
        rabbitTemplate.convertAndSend(HotelMqConstants.EXCHANGE_NAME, HotelMqConstants.DELETE_KEY, id);
        if(flag) {
            return R.ok();
        } else {
            return R.error();
        }

    }
    //根据课程id查询课程基本信息
    @GetMapping("{id}")
    public R getCourseInfo(@PathVariable String id) {
        Teacher2InfoVo teacher2InfoVo = teacher2Service.getTeacher2Info(id);
        return R.ok().data("tteacher2InfoVo",teacher2InfoVo);
    }

    //3 分页查询证书的方法
    //current 当前页
    //limit 每页记录数
//    @GetMapping("{current}/{limit}")
//    public R pageListTeacher(@PathVariable long current,
//                             @PathVariable long limit) {
//        //创建page对象
//        Page<EduTeacher2> pageTeacher = new Page<>(current,limit);
//
//        try {
//            int i = 10/0;
//        }catch(Exception e) {
//            //执行自定义异常
//            throw new GuliException(20001,"执行了自定义异常处理....");
//        }
//
//        //调用方法实现分页
//        //调用方法时候，底层封装，把分页所有数据封装到pageTeacher对象里面
//        teacher2Service.page(pageTeacher,null);
//        //总记录数
//        long total = pageTeacher.getTotal();
//        //数据list集合
//        List<EduTeacher2> records = pageTeacher.getRecords();
//
//
//        return R.ok().data("total",total).data("rows",records);
//    }

//    /**
//     * @param current
//     * @param limit
//     * @param teacherQuery
//     *  条件查询带分页的方法
//     * @return
//     */
//    @PostMapping("{current}/{limit}")
//    public R pageTeacherCondition(@PathVariable long current,@PathVariable long limit,
//                                  @RequestBody(required = false) TeacherQuery teacherQuery) {
//        //创建page对象
//        Page<EduTeacher2> pageTeacher = new Page<>(current,limit);
//
//        //构建条件
//        QueryWrapper<EduTeacher2> wrapper = new QueryWrapper<>();
//        // 多条件组合查询
//        // mybatis学过 动态sql
//        String name = teacherQuery.getName();
//        Integer level = teacherQuery.getLevel();
//        String begin = teacherQuery.getBegin();
//        String end = teacherQuery.getEnd();
//        //判断条件值是否为空，如果不为空拼接条件
//        if(!StringUtils.isEmpty(name)) {
//            //构建条件
//            wrapper.like("name",name);
//        }
//        if(!StringUtils.isEmpty(level)) {
//            wrapper.eq("level",level);
//        }
//        if(!StringUtils.isEmpty(begin)) {
//            wrapper.ge("gmt_create",begin);
//        }
//        if(!StringUtils.isEmpty(end)) {
//            wrapper.le("gmt_create",end);
//        }
//
//        //排序
//        wrapper.orderByDesc("gmt_create");
//
//        //调用方法实现条件查询分页
//        teacher2Service.page(pageTeacher,wrapper);
//
//        long total = pageTeacher.getTotal();//总记录数
//        List<EduTeacher2> records = pageTeacher.getRecords(); //数据list集合
//        return R.ok().data("total",total).data("rows",records);
//    }

    //添加证书接口的方法
    @PostMapping("")
    public R addTeacher2(@Valid @RequestBody EduTeacher2 eduTeacher) {
        boolean save = teacher2Service.save(eduTeacher);
        rabbitTemplate.convertAndSend(HotelMqConstants.EXCHANGE_NAME, HotelMqConstants.INSERT_KEY, eduTeacher.getId2());

        if(save) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    //修改课程信息
    @PutMapping("")
    public R updateCourseInfo(@Valid @RequestBody Teacher2InfoVo teacher2InfoVo) {
        teacher2Service.updateTeacher2Info(teacher2InfoVo);
        rabbitTemplate.convertAndSend(HotelMqConstants.EXCHANGE_NAME, HotelMqConstants.INSERT_KEY, teacher2InfoVo.getId2());
        return R.ok();
    }

    //根据讲师id进行查询
//    @GetMapping("{id}")
//    public R getTeacher(@PathVariable String id) {
//        EduTeacher2 eduTeacher2 = teacher2Service.getById(id);
//        return R.ok().data("teacher",eduTeacher2);
//    }

    //证书修改功能

//    @PutMapping("{id}")
//    public R updateById2(@RequestBody Teacher2Form teacher2Form, @PathVariable String id) {
//        EduTeacher2 eduTeacher2 = teacher2Service.getById(id);
//        System.out.println(eduTeacher2.getName2());
//        eduTeacher2.setName2(teacher2Form.getName2());
//        eduTeacher2.setLevel2(teacher2Form.getLevel2());
////        eduTeacher2.setCareer2(teacherForm.getCareer());
//        eduTeacher2.setIntro2(teacher2Form.getIntro2());
//        boolean save = teacher2Service.updateById(eduTeacher2);
//        if(save) {
//            return R.ok();
//        } else {
//            return R.error();
//        }
////        return R.ok();
//    }
}

