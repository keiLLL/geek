package com.atguigu.eduservice.controller;

import com.atguigu.commonutils.R;
import com.atguigu.eduservice.entity.RequestParams;
import com.atguigu.eduservice.service.TeacherEsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/eduservice/esteacher2")
public class TeacherEsController {
    @Autowired
    private TeacherEsService teacherEsService;
    @PostMapping("list")
    public R search(@RequestBody RequestParams params) {
        return teacherEsService.search(params);
    }
//用的接口
}
