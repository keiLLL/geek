//package com.atguigu.eduservice;
//
//import junit.framework.Assert;
//import org.jasypt.encryption.StringEncryptor;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@SpringBootTest
//@RunWith(SpringRunner.class)
//public class SucessTest {
//    @Autowired
//    private StringEncryptor encryptor;
//
//    @Test
//    public void getPass() {
//        String url = encryptor.encrypt("jdbc:mysql://47.104.152.166:3306/jike_database?serverTimezone=GMT%2B8");
//        String name = encryptor.encrypt("root");
//        String password = encryptor.encrypt("123456");
//        System.out.println("database url: " + url);
//        System.out.println("database name: " + name);
//        System.out.println("database password: " + password);
//        Assert.assertTrue(url.length() > 0);
//        Assert.assertTrue(name.length() > 0);
//        Assert.assertTrue(password.length() > 0);
//    }
//}
