package com.atguigu.eduservice.excel;

import com.alibaba.fastjson.JSON;
import com.atguigu.eduservice.entity.*;
import com.atguigu.eduservice.service.EduTeacher2Service;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class EsTest {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Autowired
    private EduTeacher2Service eduTeacher2Service;
    @Test
    public void TeacherBulkRequest() throws IOException {
        // 查询所有的证书数据
        List<EduTeacher2> list = eduTeacher2Service.list();

        // 1.准备Request
        BulkRequest request = new BulkRequest();
        // 2.准备参数
        for (EduTeacher2 eduTeacher2 : list) {
            // 2.1.转为HotelDoc
            Teacher2Doc teacher2Doc = new Teacher2Doc(eduTeacher2);
            // 2.2.转json
            String json = JSON.toJSONString(teacher2Doc);
            // 2.3.添加请求
            request.add(new IndexRequest("edu_teacher2").id(teacher2Doc.getId2()).source(json, XContentType.JSON));
        }

        // 3.发送请求
        restHighLevelClient.bulk(request, RequestOptions.DEFAULT);//执行这一句就空指针
    }
    @BeforeEach
    @Test
    public void setUp() {
        restHighLevelClient = new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://47.108.150.52:9200")
        ));
    }

    @AfterEach
    void tearDown() throws IOException {
        restHighLevelClient.close();
    }

}
